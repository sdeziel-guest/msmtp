#!/usr/bin/make -f

export DEB_BUILD_MAINT_OPTIONS=hardening=+all

CONFIGURE_COMMON_OPTS := --with-libgsasl --with-ssl=gnutls --with-msmtpd

FILES_MODIFIED_BY_BUILD = po/fr.po po/de.po

override_dh_auto_configure:
	# Backup files modified by autoreconf
	for file in $(FILES_MODIFIED_BY_BUILD); do \
		if [ -e $$file ] && [ ! -e $$file.keep ] ; then \
			cp $$file $$file.keep ; \
		fi \
	done
	# prepare first build: msmtp
	mkdir -p build
	ln -s $(CURDIR)/debian build/debian
	ln -s $(CURDIR)/configure build/configure
	cd build && \
		dh_auto_configure -- \
			${CONFIGURE_COMMON_OPTS} --without-libsecret
	# prepare second build: msmtp with GNOME Keyring support
	mkdir -p build-gnome
	ln -s $(CURDIR)/debian build-gnome/debian
	ln -s $(CURDIR)/configure build-gnome/configure
	cd build-gnome && \
		dh_auto_configure -- \
			${CONFIGURE_COMMON_OPTS} --with-libsecret

override_dh_auto_build:
	pod2man --center "" --date "" --release "" --section=1 \
		debian/newaliases.pod > debian/newaliases.1
	$(MAKE) -C build
	$(MAKE) -C build-gnome

override_dh_auto_install:
	$(MAKE) -C build install DESTDIR=$(CURDIR)/debian/tmp
	dh_apparmor --profile-name=usr.bin.msmtp -p msmtp

override_dh_link:
	# hook to create a symlink between /usr/share/doc/msmtp-gnome
	# and /usr/share/doc/msmtp
	rm -rf $(CURDIR)/debian/msmtp-gnome/usr/share/doc/msmtp-gnome
	dh_link

override_dh_installsystemd:
	dh_installsystemd --no-enable --name msmtpd

override_dh_auto_clean:
	rm -rf build build-gnome
	rm -f debian/newaliases.1
	dh_auto_clean
	# Restore files modified by build
	for file in $(FILES_MODIFIED_BY_BUILD); do \
			if [ -e $$file.keep ] ; then \
					rm -f $$file; \
					mv $$file.keep $$file; \
			fi \
	done
	find -iname '*.gmo' -delete
	rm -f doc/msmtp.info doc/stamp-vti doc/version.texi po/msmtp.pot

%:
	dh $@
