# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: msmtp\n"
"Report-Msgid-Bugs-To: ptitlouis@sysif.net\n"
"POT-Creation-Date: 2006-11-11 09:23+0100\n"
"PO-Revision-Date: 2006-11-11 18:29+0100\n"
"Last-Translator: Miroslav Kure <kurem@debian.cz>\n"
"Language-Team: Czech <debian-l10n-czech@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: boolean
#. Description
#: ../templates:1001
msgid "Create a system wide configuration file?"
msgstr "Vytvořit systémový konfigurační soubor?"

#. Type: boolean
#. Description
#: ../templates:1001
msgid ""
"msmtp has a sendmail emulation mode which allow to create a default system "
"account that can be used by any user."
msgstr ""
"msmtp obsahuje režim emulace sendmailu, který umožňuje vytvořit výchozí "
"systémový účet, který pak může využívat libovolný uživatel."

#. Type: string
#. Description
#: ../templates:2001
msgid "SMTP server hostname:"
msgstr "Jméno počítače se SMTP serverem:"

#. Type: string
#. Description
#: ../templates:3001
msgid "SMTP port number:"
msgstr "Číslo SMTP portu:"

#. Type: boolean
#. Description
#: ../templates:4001
msgid "Generate an envelope-from address?"
msgstr "Vytvořit adresu pro From na obálce?"

#. Type: boolean
#. Description
#: ../templates:4001
msgid ""
"msmtp can generate an envelope-from address based on the login name and the "
"\"maildomain\" configuration variable."
msgstr ""
"msmtp může vytvářet adresy pro pole From na obálce podle uživatelského jména "
"a konfigurační proměnné \"maildomain\"."

#. Type: string
#. Description
#: ../templates:5001
msgid "Domain to use for the envelope-from address:"
msgstr "Doména, kterou použít pro From na obálce:"

#. Type: boolean
#. Description
#: ../templates:6001
msgid "Use TLS to encrypt connection?"
msgstr "Použít TLS pro šifrování spojení?"

#. Type: boolean
#. Description
#: ../templates:6001
msgid ""
"Connection to remote hosts can be encrypted using TLS. This option should be "
"enabled if the remote server supports such connections."
msgstr ""
"Spojení ke vzdáleným počítačům může být šifrováno pomocí TLS. Pokud vzdálený "
"počítač taková spojení podporuje, měli byste tuto možnost povolit."
